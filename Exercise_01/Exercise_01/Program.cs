﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_01
{
    class Program
    {
        static void Main(string[] args)
        {
            string LName, FName, occupation, month;
            int age, year, bDay;
            // surname
            Console.WriteLine("well hello there!");
            Console.WriteLine("might i ask your last name?");
            LName = Console.ReadLine();
            Console.WriteLine("oh! {0} huh, what a lovely surname!", LName);

            // first name
            Console.WriteLine("and what might your firstname be?");
            FName = Console.ReadLine();
            Console.WriteLine("oh thats my favorite!");
            Console.WriteLine("--- press enter ---");
            Console.ReadLine();

            //occupation
            Console.WriteLine("ok, {0} what do you do for a living?", FName);
            occupation = Console.ReadLine();
            Console.WriteLine("interesting, {0} huh", occupation);

            // age
            Console.WriteLine("how old are you? ");
            age = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("well you look younger!");
            Console.WriteLine("--- press enter ---");
            Console.ReadLine();

            // year of birth
            Console.WriteLine("So you were born on what year?");
            year = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("ok so {0}, what month?", year);
            month = Console.ReadLine();
            Console.WriteLine("what day {0}", FName);
            bDay = Convert.ToInt32(Console.ReadLine());

            
            // conclusion
            Console.WriteLine("alright, so let me get this straight.");
            Console.WriteLine("Youre name is {0}, {1} you are {3} years old. you were born in {2} ", LName, FName, year, age);
            Console.WriteLine("on the {0} of the month of {1} ", bDay, month);
            Console.WriteLine("and you are currently {0}", occupation);
            Console.Read();
        }
    }
}
